﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SueyDict
{
    class Program
    {
        static string cpToUtf(byte[] cp866)
        {
            for (int i = 0; i < cp866.Count(); i++)
            {
              if(cp866[i] >= 0xB0) {
                  cp866[i] += 0x30;
              }
            }
            Encoding iso = Encoding.GetEncoding(866);
            Encoding utf8 = Encoding.UTF8;
            byte[] utfBytes = Encoding.Convert(iso, utf8, cp866);
            string msg = utf8.GetString(utfBytes);
            return msg.Replace("@"," ");
        }
        static byte[] utfToCp(string utfString)
        {
            Encoding iso = Encoding.GetEncoding(866);
            Encoding utf8 = Encoding.UTF8;
            byte[] cp866 = Encoding.Convert(utf8, iso, utf8.GetBytes(utfString));
            for (int i = 0; i < cp866.Count(); i++)
            {
                if (cp866[i] >= 0xE0)
                {
                    cp866[i] -= 0x30;
                }
            }
            return cp866;
        }
        static byte[] padLine(byte[] toPad) {
            byte[] res = new byte[20];
            for (int i = 0; i < 20; i++)
            {
                if (i < toPad.Count()) res[i] = toPad[i];
                else res[i] = 0x00;
            }
            return res;
        }
        static void Usage()
        {
            Console.WriteLine("Please specify args: <unpack|pack> <infile> <outfile>");
            Console.WriteLine("Usually: for packing infile is .ovl and outfile is .txt");
            Console.WriteLine("         for unpacking infile is .txt and outfile is .ovl");
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Pole Chudes dictionary unpacker by Genjitsu Gadget Lab");
            Console.WriteLine("Special thanks to Abaduaber");
            if (args.Count() < 3)
            {
                Usage();
                return;
            }

            if (args[0].Equals("unpack"))
            {
                GoodReader r = new GoodReader(File.OpenRead(args[1]));
                StreamWriter w = new StreamWriter(File.OpenWrite(args[2]));
                // blind skip first 21 bytes
                int klen = r.Read();
                byte[] lbuf = new byte[20];
                for (int i = 0; i < 20; i++)
                {
                    lbuf[i] = (byte)r.Read();
                }
                int dbcount = Convert.ToInt32(cpToUtf(lbuf).Substring(0,klen));
                int total = 0;
                string lastKey = "";
                while (!r.IsAtEnd)
                {
                    int wlen = r.Read();
                    byte[] sbuf = new byte[20];
                    for (int i = 0; i < 20; i++)
                    {
                        sbuf[i] = (byte)r.Read();
                    }
                    string werd = cpToUtf(sbuf).Substring(0, wlen);

                    wlen = r.Read();
                    sbuf = new byte[20];
                    for (int i = 0; i < 20; i++)
                    {
                        sbuf[i] = (byte)r.Read();
                    }
                    string key = cpToUtf(sbuf).Substring(0, wlen);

                    total++;
                    if (!lastKey.Equals(key))
                    {
                        w.WriteLine("[" + key + "]");
                        lastKey = key;
                    }
                    w.WriteLine(werd);
                    w.Flush();
                }
                Console.WriteLine("TOTAL: " + total.ToString());
                if (dbcount == total)
                {
                    Console.WriteLine("Database header count and extraction count match");
                }
                else
                {
                    Console.WriteLine("Database header count and extraction count MISMATCH");
                }
            }
            else if (args[0].Equals("pack"))
            {
                StreamReader r = new StreamReader(File.OpenRead(args[1]));
                GoodWriter w = new GoodWriter(File.OpenWrite(args[2]));
                string currKey = ""; string currline = "";
                int totalpairs = 0;
                while (!r.EndOfStream)
                {
                    currline = r.ReadLine();
                    if (currline.StartsWith("[") && currline.EndsWith("]"))
                    {
                        // is a key definition, skip for now
                    }
                    else
                    {
                        totalpairs++;
                    }
                }

                // Rewind
                r.BaseStream.Position = 0;

                if (totalpairs < 3)
                {
                    Console.WriteLine("ERROR: You need at least 3 key-value pairs in the word list for the game to work. Stop.");
                    return;
                }

                string pstr = totalpairs.ToString();
                w.WriteByte((byte)pstr.Length); // count of items in database length
                w.Write(padLine(utfToCp(pstr))); // count of items in database

                totalpairs = 0;
                int totalkeys = 0;
                while (!r.EndOfStream)
                {
                    currline = r.ReadLine();
                    if (currline.StartsWith("[") && currline.EndsWith("]"))
                    {
                        totalkeys++;
                        currKey = currline.Substring(2, currline.Length - 3).ToLower();
                    }
                    else
                    {
                        totalpairs++;
                        w.WriteByte((byte)currline.Length); // word length
                        w.Write(padLine(utfToCp(currline.ToLower()))); // word itself
                        w.WriteByte((byte)currKey.Length); // key length
                        w.Write(padLine(utfToCp(currKey))); // key itself
                        
                    }
                }
                Console.WriteLine("Wrote " + totalkeys.ToString() + " keys, " + totalpairs.ToString() + " key-value pairs");
            }
            else
            {
                Usage();
            }
            
        }
    }
    public class GoodReader
    {
        public readonly Stream innerStream;
        public GoodReader(Stream stream)
        {
            innerStream = stream;
        }
        public bool IsAtEnd { get { return (innerStream.Position == innerStream.Length); } }
        public int Read()
        {
            return innerStream.ReadByte();
        }
        public void Advance(int bytes)
        {
            innerStream.Position += bytes;
        }
        public int Peek()
        {
            long prevPos = innerStream.Position;
            int temp = innerStream.ReadByte();
            innerStream.Position = prevPos;
            return temp;
        }
    }
    public class GoodWriter
    {
        public readonly Stream innerStream;
        public GoodWriter(Stream stream)
        {
            innerStream = stream;
        }
        public void WriteByte(byte what)
        {
            innerStream.WriteByte(what);
            innerStream.Flush();
        }
        public void Write(byte[] what)
        {
            innerStream.Write(what, 0, what.Length);
            innerStream.Flush();
        }
    }
}
